                               WINAMPMAN
                      MP3 PLAYER Winamp Skin
                                  Version 1.3
                  Copyright (C) 2000 Bevan Sharp 


             /|||||||||||||||||\         /||||||||||||||||||||||||||||\    ||||||||||||\
             |||||         |||||        |||||               |||||          |||||
             |||||         |||||        |||||      /||||||||||||||/     |||||||||||
             |||||         |||||        |||||      |||||                   |||||
             |||||         |||||        |||||      |||||             ||||||||||||/ 
             |||||         \|||||||||||||||||/      |||||


                      Designed by: Bevan Sharp
      Email Address: smilingharlequin@hotmail.com

Mission: To design a graphically pleasing Winamp skin that not only looks cool to use, but is very easy to use.

Programs used: Notepad by Microsoft, Paint Shop Pro 5 by Jasc and Icon Edit Pro V6.0 by Hagen Wieshofer.

Influences: All those modern metalic walkmans, discmans, mobile phones and other metalic appliances. The green on black computer interface comonents were influenced by the old Apple II computers. You know the ones with the green and black monitors.

Summation: Where the present meets the past. A real modern skin with some real retro influences.

Enjoy!