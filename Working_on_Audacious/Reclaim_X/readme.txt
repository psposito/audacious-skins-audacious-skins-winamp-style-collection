Adrenalin X02 1.0
------------------------
This is the second skin of the new X02 series.
I consider this to be a crossover of my past explorations to several styles.
It was inspired by the Wipeout game series....along with a rusty touch :)

Other X-caponius' skins for winamp2x
--------------------------------
Red X 866	v2.0
X-manus		v1.2
Chiller X	v1.0
Protestor X	v1.0
Alien[X]ated	v1.0
Chiller X ltd	v1.0
Qirex 		v1.0
X-tinted	v2.0
Cosmical X	V1.4
Xenon 		v1.5
LiquidXtention	v1.0
X-tinted X09	v1.1
REversus X	v1.0
Xtd 9199	v1.0
Adrenalin X02	v1.0
Reclaim X02	v1.0

For sysmeter
--------------------------------
X tripus	v1.0
Alien[x]ated	v1.1

For ICQ PLUS
--------------------------------------
Xenon		v1.0

for sonique
-------------------------------
Xodus 84-21     v1.0

E-mail
--------------------------------------
Caponpower@yahoo.com

WebSite
------------------------------
Http:\\xhrc.homepage.com

Enjoy and distribute this skin....but dont copy any graphics and dont alter this skin.........c ya soon with a new X-caponius' skin! :) 
