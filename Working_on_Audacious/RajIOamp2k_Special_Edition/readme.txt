*** Raj.I/O.Amp.2k Special Edition

*** Created By Raj.I/O in 2000

This is a new (the first) rebuild of my first skin; Raj.I/O.Amp.2k

I strongly recomend for BEST results:
 - Go to the Visualizations Options for winamp
 - Use Spectrum analyser mode
 - Line Style
 - No Peaks
 - Thin Bands
 - The second fastest Analyser falloff rate

That'll make it look as I've intended for this skin. (Thought i'd try somethign a little different with the Vis this time)

Let me know what you all think.


*** Raj.I/O can be reached at default@email.com

PS: It's been noticed that a few of you are trying to RIP my work without cred. next time just ask me first. That ain't cool people. Not at all.