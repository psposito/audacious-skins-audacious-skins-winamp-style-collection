Kuantum winamp classic skin by KoL
Skin made to match my Kuantum Visual Style and WindowBlinds theme.

---

The images and icons are property of StudioTwentyEight. Distributing these images commercially or with any intent for monetary gains is prohibited.
NO MODIFICATIONS, PORT OR REDISTRIBUTION WITHOUT PERMISSION.


www.studiotwentyeight.com

Copyright � 2002 - 2005, StudioTwentyEight
