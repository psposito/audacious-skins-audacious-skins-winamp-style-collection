                   ***************************

                   Purple Haze Skin for Winamp

                   ***************************


This skin covers the Main Player, The Playlist, The Equalizer, The Mini Browser and The AVS.

INFo

Name: Purple Haze
Version: V1.0
Date: 25-05-2000
Short Description: Skin based around a purple cloud that I 
		   found in the background of a website.
Author: Rikki Carter (riksruin@yahoo.com)
URL: http://www.geocities.com.riksruin

If you have any comments on this skin or any other skin from my 
site then please E-mail Me (riksruin@yahoo.com).

This skin should have been downloaded from my home page 
http://www.geocities.com/riksruin If you found it on a different site
then please E-mail Me (riksruin@yahoo.com).

*********  ********  ***    ***
*********  ********  ***   ***
***   ***     **     ***  ***
***   ***     **     *** ***
*********     **     *****
*********     **     *****
******        **     *** ***
*** ***       **     ***  ***
***  ***   ********  ***   ***
***   ***  ********  ***    ***