Imperial Assault v1.0		January 28, 2000
By Stephen Leck

This is my first WinAMP skin, so I hope you like it.  I plan on making a few more skins in the future, though I
don't always have the time for it.  If you have  any questions, comments (i.e. how you liked/disliked this skin), 
or general ideas about this skin or skin making, please feel free to contact me by e-mail at smleck@sprint.ca.

Resources for this Skin:	Star Wars: Behind the Magic
			Star Wars: Yoda Stories Icons

Fonts Used:		Aurubesh
			Square721 BdEx BT
			Arial

Art Programs Used:		Paint Shop Pro 6
			Adobe Photoshop 5.5

Other Programs Used:	EasySkin (for small text and viscolor only)
			Winamp TransTrace (tranparency)
