
-----------------------------------
|  WinAMP Millenium Edition ELITE |
-----------------------------------
Software Interface (skin) for the WinAMP music player (http://www.winamp.com)
Released:  February 27, 2000



Thank you for downloading WinAMP Millenium Edition ELITE. This skin was designed to work with Winamp 2.6 but should also work with older versions without a problem.

You may contact me at:

   Homepage:     http://members.home.com/tomklepl
   MP3.com page: http://mp3.com/tomklepl
   E-mail:       tomklepl@home.com


Legal info:
The design of this skin is copyright (c) 2000 by Tom Klepl. All Rights Reserved.
This skin can be distributed freely only in its original archived form. It may not be sold, modified/altered, or packaged/combined with any other product without the written permission of the author, Tom Klepl.