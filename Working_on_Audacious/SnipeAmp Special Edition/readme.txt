
<< SNIPeAMP Special Edition >> (version 1.1)
Winamp 2.x skin 

By: Stephan Hoekstra aka AmpBurner (alkoholik@hotpop.com)
Other skins by Ampburner:Smirnoff Vodka Power Amp 2000
			 Bacardi V1.0
			 SNIPeAMP
			 SNIPeAMP Pro

The Idea of Snipeamp came from playing Unreal Tournament. One day I
woke up and thought hey, wouldn't it be nice to make a winampskin in
style of the scope of a sniper rifle. I took a look at Ut's HUD and
started skinning... 

This is the third version Which has got nothing to do with the UT hud anymore.
It's more the technology/Sniper rifle concept feeling what I was aiming for in
SnipeAmp S.E.The whole idea of snipamp is to have functionality, style
,interactivity and the feeling of actually looking through a sniper scope.
I wanted it to be more than a skin, it had to actually contribute something
to winamp instead of just making it look different. Think it's worked out
quite nice...



TO ALL SKINNERS!
Here's the deal: if you like my skin and would like to use some part of
it for your own projects, I really do not mind, under two conditions
that is... 1) Mention my name in the credits
           2) when the skin's finished, please let me know by e-mailing me
              a copy of your skin, and please mention which part u used.

You're free to modify the skin anyway you like, as long as you let me know
and give me a little bit of credit. If you're planning on modifying the skin
,but are sticking to the idea of a sniper scope, I would also appreciate it if
you would keep the name SNIPeAMP...

My thanks go out everyone who's downloaded any of my skins, and of course
the skin-sites. If you've downloaded and enjoyed this skin ,or if you have
a question or request, please let me know, coz i love feedback!


