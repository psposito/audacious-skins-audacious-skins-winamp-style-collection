-EUPHORBIA TRIGONA-
---------Unison v.2.0-----------

Creator : Marek Wojtal
E-mail : Euphorbia@giga4u.de

URL:
http://homepages.compuserve.de/marekwojtal

Skinname : Unison Brainstormed
Version : 2.0
Origin releasedate : 30th of march 2000
Updated : 12th of june 2000
Colourdepth : 16bit ( highcolour )

Changes in v.2.0 :
Version 2 "Unison Brainstormed" is now available with skinned avs-console. 
Posbar-button changed into copper-look with darkred edges.
The character of the Unison-typeface and the Euphorbia-Trigona-typeface is now also in pure 3D as you can see it in the main & the equalizer-window.
Some changes in the Playlist were done, too.
All the cool stories you mailed me were added to this file (*see BRAINSTORM-AREA at the bottom of this "readme" and enjoy it.)

Inspiration : 
This damned cold rainy days...
..waiting for spring & summer.
Music by NiN & David Bowie.
All the nice people who mailed me (*see also Thanks)

Possibillity for you to put more soul to this skin :
Please write me a short-story (Ernest Hemingway style?) to this skin. An expierience you've made you remember while looking at this skin or a fictional-story. If you don't know what I mean just download my 1914 skin at www.1001winampskins.com and read my short story in the read me.


  
FAQ-AREA :

Q. :
Why this read me is in english ?
A. :
It is most spoken (yes I know chinese is also often spoken...).
If you have problems understanding the FAQ-area you can also send me an e-mail in german or polish.

Q. : 
How do I get to know which button causes which effect ?
A. : 
If you are not used to Winamp, then please use a different skin that is descripted. After getting used to manage winamp "blind" choose my skin again.

Q. :
How do I create a skin ?
A. :
Check out different winamp-websites. Often you get useful information, tutorials and help there. Create a skin is nothing else than to create a puzzle. Winamp.exe fixes the piecec you've made.

Q. :
Can I send an e-mail if I have a question anyway ?
A. :
That was the reason I made the skin. I want a lot of e-mail ! Send questions & criticism right now to : Euphorbia@giga4u.de

Q. : 
What kind of Skintools did you used for Unison ?
A. :
Those little helpers are a big kick in the ass. Without them I would have gave up. These (Freeware-) tools are : QuickSkin 
(available at www.1001winampskins.com) , SkinWizard, SkinMaster (*see also Thanks).

Q. :
Which chord is played in the Playlist-Editor on the piano ;-)   ?
A. :
SiMONE said that it is a G-7. I trust her.
FAQ-END



Thanks to :
All the fabulous literary golems who drop me brainstorms, ideas, stories or feelings to Unison.
Every person that contacted and wrote me some words. 
John for the "QuickSkin" -Tool.
Alexander Futasz for the "Winamp Skin Wizard 1.1" -Tool.
Michael Jerves for "SkinMaster (Beta7)" -Tool.

SiMONE for first opinion, critics & patience. 
You.... for reading this and the following.

Important note :
Enjoy Unison !

Most important :
It is prohibited to use elements of this skin for other skin-projects.
It is not allowed to publish this or other skins done by me without my permission. 
It is not allowed to sell this skin in a package or separately.
It is forbidden to delete or change any of the following files in this zip-file (or wsz-file) :
balance.bmp
cButtons.bmp
eq_ex.bmp
eqMain.bmp
main.bmp
Mb.bmp
monoSter.bmp
nums_ex.bmp
playPaus.bmp
plEdit.bmp
plEdit.txt
posBar.bmp
shufRep.bmp
text.bmp
titlebar.bmp
viscolor.txt
volume.bmp
Pandoras Box.txt (this "readme" file)

Other Skins done by me
(in order of appereance) :
1. Euphorbia Trigona
2. 1914
3. SiMONE
4. WROCLAW
5. Unison Brainstormed
6. (in progress.....)

BRAINSTORM-AREA :

The following stories are feedbacks I got from all the nice people who contacted me. 
These stories are ideas, memories, or just cool brainstorming the people had while looking at Unison-skin.
Thanks again.
But now..... let some stories begin...

Brainstorm and memories by Lary Dawg :
.....i was 10 years old or so , and id just started finding out about good books like pet semetary , by stephen king reading this book , i remember "gage's mother"(i cant remember the name) telling her husband a story about when she was a child , how her sister had a horrible , crippling disease , and her mother had kept her sealed in the attic , hidden away from her guests , and even her own eyes gage's mother had to feed her sister , and she hated it in there , dusty and eery , old lace browned from years of dust , white linen soiled and musty .... the description of the room had me shivering , nearly crying with fear , even though i knew it was just a book i heard a few years later that pet semetary was available for rent on vhs tape , and i thought "a book made into a movie ... not for me" when i did finally watch it with a friend , the rendering of that scene id envisioned in my head years before was so intense i found myself lost in the movie , cringing away from the screen , 
and i think i saw a machine in that room beside the wooden wheelchair that looked exactly like your unison skin .... coincidence ?

Brainstorm by Andrea Rivolta (Italy) :
Unison ... reminds me the atmosphere of the game Blood, of some Iron Maiden's records, most of all Powerslave, of Tim Burton's films and of Bulkagow's works .... it's a gothic computer or a reed synthesizer, it's a good symbol of what I am, flesh and bits, a biomechanical golem ..... it's time to disconnect from the computer and go to recharge my batteries in my bed: unfortunately my data interface and my power supply input are on the same connector.

Weird story by "phuc you" contact: "this_town_dont_feel_mine@yahoo.com"
....i was kicking it on a boat called the S.S. minnow, We were out on a 3 hour tour, it was cool except for this dopey kinda guy always breaking shit, well anyways he steared us into some rocks and everybody DIED the most horrible death you could imagine, sept for one girl named ginger. We Boinked, she killed her self, i found a weird machine i didnt know what it did, it looked as the skin you have now does, i started turning nobs and stuff and some words popped up, they said "set date to flux; quantumn capasitorr ready" so i put in a date, and it said "key in tonal sequence" and i was like huh? so i started jamming out some kool ramms+ein tunes on the keys and the words "unison of time particals from set date to present date are aligned" everything warped and i wound up back on the boat, getting ready to fuck ginger after a dangerous boat wreck.....

Idea by Wisdom :
....it reminds me of a dashboard from some ridiculously ornate turn of the century motor car.

Narration by Vadim George Cusnarencu (Romania) contact : geovadim@yahoo.com
... I was eighteen and sober as always. I shhok my head: some blue eyes of some girl were still there. I entered my grandfather's garage. His old Corvette, covered by a thick army blanket was helding his breath for several years and i was going to end his sleep today. I pulled his rusty cover: wood shiny wheel, black leathered chairs, red paint, convertable, short gear stick. The engine roared. Free drive, blind speed, and high adrenaline. But something was missing. When I thought @ that a nice driving music bursted in the air from nowhere. It was from my car. Where from?, I wondered. In front of the gear stick I saw a golden dim shine. I took the leather driving glove and I dusted the music machine. An old bronze machine with many rusted buttons was singing a song about rain. The rain begin to fall and I covered the car. I was alone with my good music and my misterious stareo. I remembered a story of my parents about a strange music machine, bought a long time ago, from a chinese salesman, who bought it from an Atlatic island. A chinese song began to play: only harp. I don't give a damn about history. I'm interested only in music, and that was all I have ever wanted. And so I decided to start a tour of the world. The bronze machine lighted up from the inside and the song changed: "Is the end of the world as we know it"...   

BRAINSTORM-AREA-END

CONTACT-AREA :

Contact me 
right here, right now : 
Euphorbia@giga4u.de

Best Regards...
Marek W.