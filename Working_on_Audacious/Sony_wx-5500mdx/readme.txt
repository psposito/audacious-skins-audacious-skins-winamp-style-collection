Sony wx-5500mdx
version: 1.2
by: Gec Production(c), 2002
location: Ljubljana, Slovenia
Date of production: Feb.13.2002
e-mail: Geci@email.si

updates:
(March 07.2002): 	
 - eq. min. button bug fixed
 - posbar copy.bmp added (change it to posbar.bmp to use it!)

(July 17.2002):
 - cursors added
 - new vol.,bal.& eq. sliders
 - other minor graphical changes

(July 07.2003)
 - video.bmp, gen.bmp and genex.bmp added (winamp 2.9x supported)


I got the idea for this skin in Sony wx-5500mdx!!!
It took a lot of time to airbrush it and
I'm still not satisfied! Report me for bugs!!!
All windows are skined including
avs & micro!!
Mail me for Amarok and Vidamp skins!

This skin is dedicated to my 
girlfriend "Cat", thanx for 
Your support!!! I love You!

Greets to all the members of 1001winampskins.com & deviantart.com
( thanx for all the criticism...)

Expect more and better soon...

Stay Cool...

Sony is the registered trademark of Sony Corporation, Tokyo