-*** Silence ***-

-October, 2000

-- Skin was born in Adobe Photoshop 5.5
-- Cursors were born in Axialis Ax-Cursors 4.5

-- I finally finished what used to be 'Em142' but
   later became my favorite skin, so I renamed it after
   myself: 'The Silence'.

-- Fixes: Skinned the AVS & fixed/skinned some bugs :)

-- Check out my web page:
   http://www.cs.colostate.edu/~rupp

   --by Kepher Rupp
   --AKA The Silence

-- Enjoy!