Sony ReplicAmp v2.5.0

This skin has been created as an extension of the Sony ReplicAmp
1.x series by Wasit? Creations.  The original readme file text is
included at the bottom of this file.

This skin now includes support for the main window, equalizer,
playlist, minibrowser, and avs.  Unfortunately, I lost the 2.2
skins that I made last year, so this is my first re-creation of the
skin.  I hope you enjoy it!

To use, simply place this ZIP file into the winamp\skins directory

Please report problems or glitches to:  mathgod79@hotmail.com

The latest version can be found at 
http://www.chris.caseyhome.com/winamp

-Chris


Version Info
2.5.0 - Re-creation of the 2.x series, hopefully has a nicer
        look.  I have used 1.2 as a base, and extended the skin
        to the eq, playlist, minibrowser, and playlist.  I also
        tried to use more of windows' standard colors.
2.2x - My original modifications to the 1.2 series
1.2 - Original Skin from Wasit? Creations
---  ORIGINAL README FILE  ---

Thanks for downloading the Sony ReplicAmp, the first in a series of
ReplicAmps, by Wasit? Creations.  This Sony ReplicAmp was faithfully
modelled after the Sony (duh!) MZ-R30 portable MiniDisc recorder, in
my opinion the most important portable audio product to appear in
1997.  

To use, simply extract the zip file into your WinAmp\skins directory.
A subdirectory named "Sony Replicamp"  will be automatically created
for you.

Enjoy!

To report bugs, glitches, or just to tell me anything, write me at
indorock@bigfoot.com

---  END OF ORIGINAL README  ---